import requests
from bs4 import BeautifulSoup
## WebScrapping is a technique employed to extract large amounts of data from websites whereby the data is extracted and saved to a local file in your computer
# or to a database in table (spreadsheet) format.
url="https://www.codewithharry.com/"
#requests-> module of python used for Get and post requests
r=requests.get(url)
# getting the content
htmlContent=r.content
print(htmlContent)
#Parse the html
soup=BeautifulSoup(htmlContent,'html.parser')
#print(soup)
title=soup.title
print(type(title.string))

# getting all the tags and its class
paras=soup.find_all('p')
paraClass=soup.find('p')['class']
print(paraClass)

#getting element with specific class name
className=soup.find_all('p',class_='lead')
print(className)

# getting texts from the tag
text=soup.find('p').get_text()
print(text)

# getting links from the anchor
anchors=soup.find_all('a')
all_links=set()
for link in anchors:
    if(link.get('href')!='#'):
        linkText = "https://www.codewithharry.com/" + link.get('href')
        all_links.add(linkText)
        print(linkText)


# working with Comment
markup="<p>!----This is Comment-----</p>"
markSoup=BeautifulSoup(markup,'html.parser')
print(type(markSoup))

#working with specific content
navBar=soup.find(id='navbarSupportedContent')
print(navBar)

#iterating through strings of id
for elem in navBar.stripped_strings:
    print(elem)
